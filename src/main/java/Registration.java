
import java.io.IOException;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.mysql.jdbc.Driver;

import java.sql.*;

public class Registration extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uname = request.getParameter("uname");
		String email = request.getParameter("email");
		String pass1 = request.getParameter("pass1");
		String pass2 = request.getParameter("pass2");

		if (!pass1.equals(pass2)) {
			response.sendRedirect("register.jsp");
		} else if (!email.contains("@") || !email.contains(".")) {
			response.sendRedirect("register.jsp");
		} else if (uname.equals(null) || email.equals(null) || pass1.equals(null) || pass2.equals(null)) {
			response.sendRedirect("register.jsp");
		} else if (!uname.equals(null) && !email.equals(null) && !pass1.equals(null) && !pass2.equals(null)) {
				response.sendRedirect("index.jsp");
		}

	}

}
