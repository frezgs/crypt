package crypt;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Settings extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public String vtcTotal;
	public String vtcWallet;
	public String btcTotal;
	public String ltcTotal;
	public String ethTotal;
	
	public void onPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	
	public String setVtcTotal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		return vtcTotal;
	}
	
	public String getVtcTotal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String setVtcWallet = request.getParameter("address");
		String vtcAddress = ("http://explorer.vertcoin.info/ext/getbalance/" + vtcWallet);
				
		Document doc = Jsoup.connect(vtcAddress).get();
		Elements vtcAmount = doc.select("body");
		vtcTotal = vtcAmount.text();
		System.out.println(vtcTotal);
		if (!vtcTotal.equals(null)) {
			request.setAttribute("vtcTotal", vtcTotal);
		}
		request.getRequestDispatcher("crypt/dashboard.jsp").forward(request, response);
		return this.vtcTotal;
	}
	
	public String getBtcTotal() {
		return this.btcTotal;
	}
	
	public String getLtcTotal() {
		return this.ltcTotal;
	}
	
	public String getEthTotal() {
		return this.ethTotal;
	}
	
	public String getVtcAddres(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String vtcWallet = "testWallet0215321t43253";//request.getParameter("address");
		return this.vtcWallet;
	}
}
