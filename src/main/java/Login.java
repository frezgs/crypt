
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uname = request.getParameter("uname");
		String pass = request.getParameter("pass");
		
		if(uname.equals("root") && pass.equals("root")) {
			HttpSession session = request.getSession();
			session.setAttribute("username", uname);
			response.sendRedirect("dashboard.jsp");
		}else {
			response.sendRedirect("index.jsp");
		}
	}

}
