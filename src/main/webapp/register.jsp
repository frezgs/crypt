<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="crypt.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register</title>

<style>
@import url(http://fonts.googleapis.com/css?family=Exo:100,200,400);
@import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);
body{
    position: fixed; 
    margin: 0px;
    overflow-y: scroll;
    width: 100%;
	top: -20px;
	left: -20px;
	right: -40px;
	bottom: -40px;
	width: auto;
	height: auto;
	background-color: #1f2228;
	background-size: cover;
	-webkit-filter: blur(0px);
	
}
p {
	color: red;
}
.header{
	position: absolute;
	top: calc(50% - 35px);
	left: calc(50% - 323px);
	
}
.header div{
	float: left;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 40px;
	font-weight: 200;
}
.header div span{
	color: #F29E20;
}
.login{
	position: absolute;
	top: calc(50% - 75px);
	left: calc(50% - 50px);
	height: 150px;
	width: 350px;
	padding: 10px;
	
}
input[type=text]{
	width: 250px;
	height: 30px;
	background: transparent;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
	margin-top: 10px;
}
input[type=password]{
	width: 250px;
	height: 30px;
	background: transparent;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
	margin-top: 10px;
}
input[type=submit]{
	width: 252px;
	height: 35px;
	background: #fff;
	border: 1px solid #fff;
	cursor: pointer;
	border-radius: 2px;
	color: #F29E20;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 6px;
	margin-top: 10px;
}
</style>
</head>

<body>
	<div class="grad"></div>
	<div class="header">
		<div>Crypto<span>Tracker</span></div>
		<script src="java/Registration.java"></script>
	</div>

<div class="login">
	<form action="Registration">
	<input type="text" placeholder="Username" name="uname"><br>
	<input type="text" placeholder="Email" name="email"><br>
	<input type="password" placeholder="Enter Password" name="pass1"><br>
	<input type="password" placeholder="Verify Password" name="pass2"><br>
	<input type="submit" value="Register"><br>
	</form>
</div>
	
</body>
</html>