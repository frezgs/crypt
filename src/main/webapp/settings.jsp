<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="crypt.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style>
body { 
	background-color: #1f2228;
	margin: 0px;
}
.topnav {
    background-color: #2b2e36;
    overflow: hidden;
}
.topnav a {
    float: right;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 17px;
}
.topnav a:hover {
    background-color: #ddd;
    color: black;
}
.active {
    background-color: #4CAF50;
    color: white;
}
.topSpace {
    overflow: hidden;
    height: 75px;
    background-color: transparent;
}
.spacing {
    overflow: hidden;
    background-color: transparent;
}
.column {
    float: left;
    padding: 10px;
}
.column h2 {
    text-align: center;
    color: white;
    font-size: 30px;
    font-weight: bold;
    text-align: center;
    text-decoration: none;
    border-bottom:2px solid #F29E20;
}
.column p {
    text-align: left;
    color: white;
    font-size: 23px;
    font-weight: bold;
    text-decoration: none;
}
.column.space {
    width: 4.5%;
}
.column.dashboard {
    width: 33%;
    height: 300px;
    margin-top: 25px;
    margin-left: 25px;
    overflow: hidden;
    background: #2b2e36;
}
.column.user {
    width: 34%;
    margin-left:25px;
    margin-top: 25px;
    background: #2b2e36;
}
input[type=text]{
	margin-left: auto;
    margin-right: auto;
	width: 400px;
	height: 30px;
	background: transparent;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
}
input[type=submit]{
	width: 150px;
	height: 35px;
	background: transparent;
	border: 1px solid #fff;
	cursor: pointer;
	border-radius: 2px;
	color: white;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 6px;
	margin-top: 10px;
}
</style>
</head>

<body>
<%
Settings se = new Settings();
%>
	<div class="topnav" id="myTopnav">
		<a href="Logout" class="active">Logout</a> <a href="dashboard.jsp">Dashboard</a>
	</div>

	<div class="column dashboard">
		<p>Select what currency you would like to register</p>
		<select>
			<option value="Vertcoin">Vertcoin</option>
			<option value="Litecoin">Litecoin</option>
			<option value="Etherium">Etherium</option>
			<option value="Bitcoin">Bitcoin</option>
		</select>
		<p>Enter wallet address</p>
		<input type="text" placeholder="(Ex:)VyJ9NKD8W8WcLZgLorg37L7hKBxia7pz3x" name="address"><br>
		<input type="submit" value="Submit"><br>
	</div>
	
	<div class="column user">
	<p>Saved wallets:</p>
	<p>Vertcoin: <%%> </p>
	</div>
</body>






</html>