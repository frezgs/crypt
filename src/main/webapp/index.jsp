<html>

<head>
<meta charset="UTF-8">

  <title>Login</title>

    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="331527985624-9bmchtast2drv0n8gin55u105p6bbn53.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <style>
@import url(http://fonts.googleapis.com/css?family=Exo:100,200,400);
@import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);
body{
    position: fixed; 
    margin: 0px;
    overflow-y: scroll;
    width: 100%;
	top: -20px;
	left: -20px;
	right: -40px;
	bottom: -40px;
	width: auto;
	height: auto;
	background-color: #1f2228;
	background-size: cover;
	-webkit-filter: blur(0px);
	
}
.header{
	position: absolute;
	top: calc(50% - 35px);
	left: calc(50% - 323px);
	
}
.header div{
	float: left;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 40px;
	font-weight: 200;
}
.header div span{
	color: #F29E20;
}
.login{
	position: absolute;
	top: calc(50% - 75px);
	left: calc(50% - 50px);
	height: 150px;
	width: 350px;
	padding: 10px;
	
}
input[type=text]{
	width: 250px;
	height: 30px;
	background: transparent;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
}
input[type=password]{
	width: 250px;
	height: 30px;
	background: transparent;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
	margin-top: 10px;
	margin-bottom: 15px;
}
input[type=submit]{
	width: 252px;
	height: 35px;
	background: #fff;
	border: 1px solid #fff;
	cursor: pointer;
	border-radius: 2px;
	color: #F29E20;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
}
</style>
</head>

<body>
	<div class="grad"></div>
	<div class="header">
		<div>Crypto<span>Tracker</span></div>
	</div>

<div class="login">
	<form action="Login">
		<input type="text" placeholder="Username" name="uname"><br>
		<input type="password" placeholder="Password" name="pass"><br>
		<input type="submit" value="Login"><br>
	</form>
	<form action="Register">
		<input type="submit" value="Register"><br>
	</form>
</div>
	
</body>

</html>