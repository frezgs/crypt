<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="crypt.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Dashboard</title>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Currency', 'Bitcoin', 'Etherium', 'Litecoin', 'Vertcoin'],
          ['Bitcoin', 1000, 0, 0, 0],
          ['Eherium', 0, 460, 0, 0],
          ['Litecoin', 0, 0, 300, 0],
          ['Vertcoin', 0, 0, 0, 92.5]
        ]);

        var options = {
        backgroundColor: 'transparent',
          chart: {
            title: '',
            subtitle: '',
          },
          colors: ['gold', 'purple', 'grey', 'green']
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
     </script>
    
<style>
body {
	background-color: #1f2228;
	margin: 0px;
}

.topnav {
	background-color: #2b2e36;
	overflow: hidden;
}

.topnav a {
	float: right;
	display: block;
	color: #f2f2f2;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	font-size: 17px;
}

.topnav a:hover {
	background-color: #ddd;
	color: black;
}

.active {
	background-color: #4CAF50;
	color: white;
}

.column {
	float: left;
}

.column h2 {
	text-align: center;
	color: white;
	font-size: 30px;
	font-weight: bold;
	text-align: center;
	text-decoration: none;
	border-bottom: 2px solid #F29E20;
}

.column p {
	text-align: left;
	color: white;
	font-size: 15px;
	font-weight: bold;
	text-decoration: none;
}

.column.space {
	margin-left: 17px;
	margin-top: -15px;
	
}

.column.sidebar {
	width: 15vw;
	height: 49vw;
	margin-left: 25px;
	margin-right: 25px;
	margin-top: 25px;
	margin-bottom: 25px;
	background: #2b2e36;
}

.column.dashboard {
	width: 42vw;
	height: 49vw;
	overflow: hidden;
	margin-top: -45px;
	background: #2b2e36;
}

.column.dashboardgraph {
	width: calc(75%);
	background: #2b2e36;
}

.column.dashboardtext {
	width: 25%;
	background: #2b2e36;
}

.column.user {
	width: 48%;
	height: 280vw;
	margin-top: 25px;
	background: #2b2e36;
}

.column.price {
	width: 26%;
	height: 280px;
	margin-left: 25px;
	margin-top: 25px;
	background: #2b2e36;
}

.row:after {
	content: "";
	display: table;
	clear: both;
}

input[type=submit] {
	float: right;
	display: block;
	background: transparent;
	background-opacity: 0.5;
	color: white;
	text-align: center;
	padding: 8px 16px;
	text-decoration: none;
	font-size: 17px;
	border-radius: 4px;
}

#wrapper {
	width: 80%;
	margin: 50px auto 0px auto;
	padding: 20px 20px 20px 20px;
}
</style>
</head>

<body>
	<%
	Settings se = new Settings();
	%>
	<div class="topnav" id="myTopnav">
		<a href="Logout" class="active">Logout</a> <a href="settings.jsp">Settings</a>
	</div>

	<div class="column sidebar"> 

		<h2>User Stats</h2>
		<p>Welcome user,</p>
		<p>Total balance (USD):</p>
		<p>Current balance:</p>
		
		<div class="topSpace"><p></p></div>
		
		<h2>User Stats</h2>
		<p>Todays prices:</p>
		<div class="column space">
		<p>Bitcoin: $830</p>
		<p>Litecoin: <%out.print(se.ltcTotal);%></p>
		<p>Etherium: $830</p>
		</div>
		
	</div>

	<div id="wrapper">
		<div class="column dashboard">
			<h2>Dashboard</h2>
			<div id="columnchart_material" style="width: 100%; height: 70%;"></div>
		</div>

	</div>

</body>

</html>