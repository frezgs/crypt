<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="crypt.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<script type="text/javascript"
	src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          	title: 'My Daily Activities',
        	backgroundColor: 'transparent'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
<style>
body {
	background-color: #1f2228;
	margin: 0px;
}
<!-- navigation -->
.topnav {
	background-color: #2b2e36;
	overflow: hidden;
}
.topnav a {
	float: right;
	display: block;
	color: #f2f2f2;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	font-size: 17px;
}
.topnav a:hover {
	background-color: #ddd;
	color: black;
}
.active {
	background-color: #4CAF50;
	color: white;
}

<!-- the body of the page -->

.column.dashboard {
	overflow: hidden;
	background: #2b2e36;
}

</style>
</head>

<body>

	<div class="topnav" id="myTopnav">
 		<a href="Logout" class="active">Logout</a>
  		<a href="settings.jsp">Settings</a>
	</div>
	
	<div class="column dashboard">
		<h2>Dashboard</h2>
		<div id="piechart" style="width: calc(130%); height: calc(100%);"></div>
	</div>

</body>

</html>